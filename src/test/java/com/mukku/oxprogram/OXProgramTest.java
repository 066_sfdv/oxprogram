/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.oxprogram;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'O', '-', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', '-', 'O'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'O', 'O', 'O'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerOinX1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckPlayerOinX2Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', 'O', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckPlayerOinXWin() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
    
    @Test
    public void testCheckPlayerOWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'O', 'O', 'O'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        int col = 3;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, row, col));
    }
    
    @Test
    public void testCheckDraw() {
        char table[][] = {{'O', 'X', 'O'}, 
                                 {'O', 'X', 'O'}, 
                                 {'X', 'O', 'X'}};
        int count=8;
        assertEquals(true, OXProgram.checkDraw(count));
    }
    
    @Test
    public void testSetTable() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'O', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        int col = 3;
        assertEquals(true, OXProgram.setTable(table, row, col));
    }
    
    @Test
    public void testCheckPlayerOCol1NoWin() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'O', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(false, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerORow1NoWin() {
        char table[][] = {{'O', '-', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(false, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerOinXNoWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(false, OXProgram.checkX(table, currentPlayer));
    }
    
    @Test
    public void testNoSetTableDup() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'O', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        int col = 1;
        assertEquals(false, OXProgram.setTable(table, row, col));
    }
    
    @Test
    public void testNoSetTableOORange() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'O', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 5;
        int col = 8;
        assertEquals(false, OXProgram.setTable(table, row, col));
    }
    
    @Test
    public void testCheckPlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'X', '-', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', 'X'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckPlayerXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerXRow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'X', 'X'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerXRow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckPlayerXinX1Win() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckPlayerXinX2Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', 'X', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckPlayerXinXWin() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
    
    @Test
    public void testCheckPlayerXWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'X', 'X'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;
        int col = 3;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, row, col));
    }
    
}
